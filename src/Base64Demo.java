import java.util.Base64;

public class Base64Demo {
    public static void main(String[] args) {
        String originalMessage = "this is important message!";
        String encodedMessage = new String(
                Base64.getEncoder().encode(originalMessage.getBytes())
        );

        System.out.println(encodedMessage);
        String newEncodedMessage = "dGhpcyBpcyBpbXBvcnRhbnQgbWVzc2FnZSE=";
    String decodedMessage =  new String(
            Base64.getDecoder().decode(newEncodedMessage)
    );
        System.out.println("Decoded Message is : "+decodedMessage);

    }
}
