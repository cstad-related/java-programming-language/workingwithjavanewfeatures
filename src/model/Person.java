package model;

import java.util.UUID;

public class Person {
    private UUID id = UUID.randomUUID();
    private String name;
    private String gender;
    private String province;

    public Person(){
    }
    public Person(UUID id, String name, String gender, String province){
        this.id = id;
        this.name = name;
        this.gender = gender;
        this.province = province;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", gender='" + gender + '\'' +
                ", province='" + province + '\'' +
                '}';
    }
}
