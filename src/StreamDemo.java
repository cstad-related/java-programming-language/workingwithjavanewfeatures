import model.Person;

import java.util.*;
import java.util.stream.Collectors;

public class StreamDemo {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        List<Person> personList = new ArrayList<>() {{
            for (int i = 1; i <= 10; i++) {
                add(new Person(
                        UUID.randomUUID(),
                        "username" + i,
                        (i % 2 == 0) ? "male" : "female",
                        (i % 2 == 0) ? "siem reap" : "btb"));
            }

        }};
        // map,foreach , filter
        Person personi = new Person();
        personi.setId(UUID.fromString("858cf768-5596-4f48-9bb9-23a51b4a24e2"));
        personList.add(personi);
        //  personList.stream().forEach(person-> System.out.println(person));
        // personList.forEach(person-> System.out.println(person));
        personList.forEach(System.out::println);

        // search for people that comes from sr
        var provinces = personList.stream().map(Person::getProvince)
                .collect(Collectors.toList());
        System.out.println("provinces" + provinces);

        // want to only get the names
        // genders
//        var femalePeople = personList.stream()
//                .filter(person -> person.getGender().equalsIgnoreCase("female"))
//                .map(Person::getName)
//                .toList();
//
//        femalePeople.forEach(System.out::println);


        String searchID="858cf768-5596-4f48-9bb9-23a51b4a24e9";
        Optional<Person> optionalPerson = personList.stream()
                .filter(person -> person.getId().equals(UUID.fromString(searchID)))
                .findFirst();


        if(optionalPerson.isPresent()){
            System.out.println("Person Data is :"+optionalPerson.get());
        }else {
            System.out.println("Person with ID "+ searchID + " doesn't exist ! ");
        }


    }
}
