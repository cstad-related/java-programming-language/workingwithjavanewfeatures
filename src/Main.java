import service.IAuthenticationService;
import utils.MainUtils;


public class Main {
    public static void main(String[] args) {
   /*     IAuthenticationService auth = ()-> System.out.println("This is login method");
        auth.login();*/
//        var obj = new Main();
/*
        IAuthenticationService auth =(username, password) -> {
            System.out.println("This is for multiple statement.....");
            System.out.println("This message is : " + username + "  "+password);
        };
*/
//        String adminUsername = "hello", adminPassword = "hello";
//        IAuthenticationService auth = (username, password) -> {
//            System.out.println("Helloeveryone");
//            return true;
//        };

// reference to static method !
 /*       IAuthenticationService auth = MainUtils::showMessage;
        auth.login();
        // reference to instance method
        MainUtils mainUtils = new MainUtils();
        IAuthenticationService auth2 = mainUtils::showMethod2;
        auth2.login();
        // reference to instance method with anonymous object
        IAuthenticationService auth3 = new MainUtils()::showMethod2;
        */
//        IAuthenticationService auth4 = MainUtils::new;
//        auth4.login();

// more compact  and shorter
        IAuthenticationService auth = MainUtils::showMessage;
        auth.login("username","password");
        auth.testing();
    }
}
